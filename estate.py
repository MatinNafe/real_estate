from real_estate.agent import Supervisor


class BaseProperty():
    def __init__(self, area, room_number, parking, address, **kwargs):
        super().__init__(**kwargs)
        self.area = area
        self.room_number = room_number
        self.parking = parking
        self.address = address

    @classmethod
    def prompt(cls):
        area = input("please enter area ")
        result = {
            'area' : area
        }
        return result

class Apartment(BaseProperty):
    def __init__(self, floor, elevator, balcony, **kwargs):
        super().__init__(**kwargs)
        self.floor = floor
        self.elevator = elevator
        self.balcony = balcony


class House(BaseProperty):
    def __init__(self, pool, yard, **kwargs):
        super().__init__(**kwargs)
        self.pool = pool
        self.yard = yard


class Rental:
    def __init__(self, pre_paid, monthly, **kwargs):
        super().__init__(**kwargs)
        self.pre_paid = pre_paid
        self.monthly = monthly


class Purchasable:
    def __init__(self, cost, **kwargs):
        super().__init__(**kwargs)
        self.cost = cost


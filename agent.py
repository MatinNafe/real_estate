class BaseUser():
    def __init__(self, username, password, first_name, last_name, email, **kwargs):
        super().__init__(**kwargs)
        self.username = username
        self.password = password
        self.first_name = first_name
        self.last_name = last_name
        self.email = email


class Supervisor(BaseUser):
    agents_list = list()
    properties_dict = dict()
    deals_list = list()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class Agent(BaseUser):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        Supervisor.agents_list.append(self)
        self.properties_list = list()
        self.deals_list = list()

    def create_property(self, **kwargs):
        instance = None
        self.properties_list.append(instance)
        Supervisor.properties_dict[self.username].append(instance)

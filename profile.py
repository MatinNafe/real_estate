from estate import Apartment, Rental, Purchasable, House


class ApartmentRental(Apartment, Rental):
    pass


class ApartmentPurchase(Apartment, Purchasable):
    pass


class HouseRental(House, Rental):
    pass


class HousePurchase(House, Purchasable):
    pass
